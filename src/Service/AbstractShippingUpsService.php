<?php

namespace Bitkorn\ShippingUps\Service;

use SimpleXMLElement;

abstract class AbstractShippingUpsService extends AbstractUpsService
{

    /**
     * @var SimpleXMLElement
     */
    protected $shipmentXML;

    /**
     * @var SimpleXMLElement
     */
    protected $packageXML;

    /**
     * @param SimpleXMLElement $requestXML
     */
    protected function setShipment(SimpleXMLElement &$requestXML): void
    {
        $this->shipmentXML = $requestXML->addChild('Shipment');

        if ($this->upsConfig['negotiated_rates'] === true) {
            $rateInformation = $this->shipmentXML->addChild('RateInformation');
            $rateInformation->addChild('NegotiatedRatesIndicator', '');
        }

        $shipper = $this->shipmentXML->addChild('Shipper');
        $shipper->addChild('Name', $this->upsConfig['shipment']['name']);
        $shipper->addChild('ShipperNumber', $this->upsConfig['shipment']['number']);
        $shipper->addChild('PhoneNumber', $this->upsConfig['shipment']['phone']);
        if (!empty($this->upsConfig['shipment']['tax_id'])) {
            $shipper->addChild('TaxIdentificationNumber', $this->upsConfig['shipment']['tax_id']);
        }
        $shipperAddress = $shipper->addChild('Address');
        $shipperAddress->addChild('AddressLine1', $this->upsConfig['shipment']['address']['address_line_1']);
        $shipperAddress->addChild('City', $this->upsConfig['shipment']['address']['city']);
        $shipperAddress->addChild('PostalCode', $this->upsConfig['shipment']['address']['postal_code']);
        $shipperAddress->addChild('CountryCode', $this->upsConfig['shipment']['address']['country_code']);
    }

    protected function validShipper(): bool
    {
        if (!isset($this->shipmentXML) || !$this->shipmentXML instanceof SimpleXMLElement) {
            return false;
        }
        return true;
    }

    /**
     * Service Codes - European Union:
     * 70 UPS Access Point Economy      Shipments within the European Union
     * 08 UPS Expedited                 Shipments originating in the European Union
     * 07 UPS Express                   Shipments originating in the European Union
     * 11 UPS Standard                  Shipments originating in the European Union
     * 54 UPS Worldwide Express Plus    Shipments originating in the European Union
     * 65 UPS Worldwide Saver           Shipments originating in the European Union
     * 74 UPS Express®12:00             German Domestic Shipments
     * 72 UPSTM Economy DDP             Shipments originating in the European Union (only Shipping ...Rating N/A)
     * 17 UPSTM Economy DDU             Shipments originating in the European Union (only Shipping ...Rating N/A)
     *
     * @param string $code
     * @param string $desc
     */
    public function setShipmentService(string $code = '11', string $desc = 'UPS Standard'): void
    {
        if (!$this->validShipper()) {
            throw new \BadMethodCallException(__CLASS__ . '()->' . __FUNCTION__ . '() you will call setShipper() first.');
        }
        if (!in_array($code, array_keys($this->upsConfig['service_codes_supported']))) {
            throw new \BadMethodCallException(__CLASS__ . '()->' . __FUNCTION__ . '() service code is not supported. Codes supported: ' . explode(', ', $this->upsConfig['service_codes_supported']));
        }
        $service = $this->shipmentXML->addChild('Service');
        $service->addChild('Code', $code);
        $service->addChild('Description', $desc);
    }

    /**
     * @param string $companyName
     * @param string $phone
     * @param string $addressLine1
     * @param string $city
     * @param string $postalCode
     * @param string $countryCode
     */
    public function setShipmentShipFrom(string $companyName, string $phone, string $addressLine1, string $city, string $postalCode, string $countryCode): void
    {
        if (!$this->validShipper()) {
            throw new \BadMethodCallException(__CLASS__ . '()->' . __FUNCTION__ . '() you will call setShipper() first.');
        }
        $shipFrom = $this->shipmentXML->addChild('ShipFrom');
        $shipFrom->addChild('CompanyName', $companyName);
        $shipFrom->addChild('PhoneNumber', $phone);
        $shipFromAddress = $shipFrom->addChild('Address');
        $shipFromAddress->addChild('AddressLine1', $addressLine1);
        $shipFromAddress->addChild('City', $city);
        $shipFromAddress->addChild('PostalCode', $postalCode);
        $shipFromAddress->addChild('CountryCode', $countryCode);
    }

    public function setShipmentShipFromDefault(): void
    {
        $this->setShipmentShipFrom(
            $this->upsConfig['shipment']['name'],
            $this->upsConfig['shipment']['phone'],
            $this->upsConfig['shipment']['address']['address_line_1'],
            $this->upsConfig['shipment']['address']['city'],
            $this->upsConfig['shipment']['address']['postal_code'],
            $this->upsConfig['shipment']['address']['country_code']
        );
    }

    /**
     * @param string $companyName
     * @param string $phone
     * @param string $email
     * @param string $addressLine1
     * @param string $city
     * @param string $postalCode
     * @param string $countryCode
     */
    public function setShipmentShipTo(string $companyName, string $phone, string $email, string $addressLine1, string $city, string $postalCode, string $countryCode): void
    {
        if (!$this->validShipper()) {
            throw new \BadMethodCallException(__CLASS__ . '()->' . __FUNCTION__ . '() you will call setShipper() first.');
        }
        $shipTo = $this->shipmentXML->addChild('ShipTo');
        $shipTo->addChild('CompanyName', $companyName);
        $shipTo->addChild('PhoneNumber', $phone);
        $shipTo->addChild('EMailAddress', $email);
        $shipToAddress = $shipTo->addChild('Address');
        $shipToAddress->addChild('AddressLine1', $addressLine1);
        $shipToAddress->addChild('City', $city);
        $shipToAddress->addChild('PostalCode', $postalCode);
        $shipToAddress->addChild('CountryCode', $countryCode);
    }

    /**
     * Package/PackagingType:
     * 01 = UPS Letter
     * 02 = Customer Supplied Package
     * 03 = Tube
     * 04 = PAK
     * 21 = UPS Express Box
     * 24 = UPS 25KG Box
     * 25 = UPS 10KG Box
     * 30 = Pallet
     * 2a = Small Express Box
     * 2b = Medium Express Box
     * 2c = Large Express Box
     * 56 = Flats 57 = Parcels
     * 58 = BPM 59 = First Class
     * 60 = Priority
     * 61 = Machinables
     * 62 = Irregulars
     * 63 = Parcel Post
     * 64 = BPM Parcel
     * 65 = Media Mail
     * 66 = BPM Flat
     * 67 = Standard Flat
     *
     * Package/PackageWeight/UnitOfMeasurement:
     * LBS = Pounds
     * KGS = Kilograms
     * OZS = Ounces
     * 00 = Metric Unit of Measurements
     * 01 = English Unit of Measurements
     *
     * @param float $weight In grams
     * @param string $desc
     */
    public function addShipmentPackage(float $weight, string $desc = ''): void
    {
        $weight = $weight / 1000;
        $this->packageXML = $this->shipmentXML->addChild('Package');
        $this->packageXML->addChild('Description', $desc);

        $packagingType = $this->packageXML->addChild('PackagingType');
        $packagingType->addChild('Code', '02');
        $packagingType->addChild('Description', 'UPS Package');

        $packageWeight = $this->packageXML->addChild('PackageWeight');
        $unitOfMeasurement = $packageWeight->addChild('UnitOfMeasurement');
        $unitOfMeasurement->addChild('Code', 'KGS');
        $packageWeight->addChild('Weight', $weight);
    }

    /**
     * @return false|resource
     */
    protected function prepareCurl()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpointUrl . $this->endpointUrlAppendage);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return $ch;
    }
}
