<?php

namespace Bitkorn\ShippingUps\Service;

use Bitkorn\Trinket\Service\AbstractService;
use SimpleXMLElement;

abstract class AbstractUpsService extends AbstractService
{
    /**
     * @var array
     */
    protected $upsConfig;

    /**
     * @var string UPS license number
     */
    protected $accessLicenseNumber = 'Add License Key Here';

    /**
     * @var string
     */
    protected $userId = 'Add User Id Here';

    /**
     * @var string
     */
    protected $password = 'Add Password Here';

    /**
     * @var string Base URL
     */
    protected $endpointUrl = 'Add URL Here';

    /**
     * @var string Appendage for different services, e.g. '/Rate' | '/ShipConfirm'
     */
    protected $endpointUrlAppendage;

    /**
     * @var SimpleXMLElement Base access for all kinds of requests.
     */
    protected $accessRequestXML;

    /**
     * 1. Sets the UPS config array.
     * 2. call $this->computeEndpointUrl()
     * 3. call $this->initAccessRequestXML()
     *
     * @param array $upsConfig
     */
    public function setUpsConfig(array $upsConfig): void
    {
        $this->upsConfig = $upsConfig;
        $this->accessLicenseNumber = $this->upsConfig['access_license_number'];
        $this->userId = $this->upsConfig['user_id'];
        $this->password = $this->upsConfig['password'];
        $this->computeEndpointUrl();
        $this->initAccessRequestXML();
    }

    /**
     * Switch UPS configs 'entpoint_mode' (development | production) to set endpointUrl.
     * Called in $this->>setUpsConfig()
     */
    protected function computeEndpointUrl(): void
    {
        switch ($this->upsConfig['entpoint_mode']) {
            case 'production':
                $this->endpointUrl = $this->upsConfig['endpoint_url'];
                break;
            case 'development':
            default:
                $this->endpointUrl = $this->upsConfig['endpoint_url_dev'];
                break;
        }
    }

    /**
     * Sets $this->accessRequestXML with AccessLicenseNumber, UserId & Password from locale members.
     * Called in $this->>setUpsConfig()
     */
    protected function initAccessRequestXML(): void
    {
        $this->accessRequestXML = new SimpleXMLElement('<AccessRequest></AccessRequest>');
        $this->accessRequestXML->addChild('AccessLicenseNumber', $this->accessLicenseNumber);
        $this->accessRequestXML->addChild('UserId', $this->userId);
        $this->accessRequestXML->addChild('Password', $this->password);
    }
}
