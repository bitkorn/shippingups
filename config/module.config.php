<?php

namespace Bitkorn\ShippingUps;

use Bitkorn\ShippingUps\Factory\Service\Kit\AddressValidationUpsServiceKitFactory;
use Bitkorn\ShippingUps\Factory\Service\Kit\RatingUpsServiceKitFactory;
use Bitkorn\ShippingUps\Factory\Service\Kit\ShippingUpsServiceKitFactory;
use Bitkorn\ShippingUps\Factory\Service\Kit\TrackingUpsServiceKitFactory;
use Bitkorn\ShippingUps\Service\Kit\AddressValidationUpsServiceKit;
use Bitkorn\ShippingUps\Service\Kit\RatingUpsServiceKit;
use Bitkorn\ShippingUps\Service\Kit\ShippingUpsServiceKit;
use Bitkorn\ShippingUps\Service\Kit\TrackingUpsServiceKit;

return [
    'router' => [
        'routes' => [],
    ],
    'controllers' => [
        'factories' => [],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [
            /*
             * UPS kits
             */
            AddressValidationUpsServiceKit::class => AddressValidationUpsServiceKitFactory::class,
            RatingUpsServiceKit::class => RatingUpsServiceKitFactory::class,
            ShippingUpsServiceKit::class => ShippingUpsServiceKitFactory::class,
            TrackingUpsServiceKit::class => TrackingUpsServiceKitFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helper_config' => [
        'factories' => [],
        'invokables' => [],
        'aliases' => [],
    ],
    'view_manager' => [
        'template_map' => [],
        'template_path_stack' => [],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'bitkorn_shop_shipping_ups' => [
        'access_license_number' => '',
        'user_id' => '',
        'password' => '',
        'entpoint_mode' => 'development', // development | production
        'endpoint_url' => 'https://onlinetools.ups.com/ups.app/xml/',
        'endpoint_url_dev' => 'https://wwwcie.ups.com/ups.app/xml/',
        'shipment' => [
            'name' => '',
            'number' => '', // UPS Kundennummer
            'phone' => '',
            'tax_id' => '',
            'address' => [
                'address_line_1' => '',
                'city' => '',
                'postal_code' => '',
                'country_code' => '', // DE, EN etc.
            ],
        ],
        /*
         * UPS can calculate shipping rates only from/to the same country.
         * This is the price for different from/to countries.
         */
        'rating_price_different_origin_country' => 0,
        /*
         * negotiated rates for an authorized account?
         */
        'negotiated_rates' => true,
        'service_codes_supported' => [
            '07' => 'UPS Express',
            '08' => 'UPS Expedited',
            '11' => 'UPS Standard',
            '54' => 'UPS Worldwide Express Plus',
            '65' => 'UPS Worldwide Saver',
        ],
    ]
];
