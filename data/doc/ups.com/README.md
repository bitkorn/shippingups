# UPS API Integration

## Links

[Tool Center für Entwickler](https://www.ups.com/de/de/help-center/technology-support/developer-resource-center.page)

[UPS Kit für Entwickler (APIs zum Download)](https://www.ups.com/upsdeveloperkit?loc=de_DE)

[weitere API Übersichtsseite](https://www.ups.com/de/de/services/technology-integration/developer-api.page)

[Anweisungen für Entwickler](https://www.ups.com/de/de/help-center/sri/developer-instruct.page)

#### UPS Service Codes
[UPS_Service_Codes](https://www.ups.com/worldshiphelp/WS14/ENU/AppHelp/SHIPUPS.htm#Codes/UPS_Service_Codes.htm)

Die Codes stimmen nicht mit denen im Beispielcode überein.

[stackoverflow.com/ups-rates-api-how-to-get-service-description](https://stackoverflow.com/questions/13761965/ups-rates-api-how-to-get-service-description)

Darin ist die gute Idee im Magento Code zu gucken.

[gist.github.com - UPS Service Codes & Package Type Codes in the UK](https://gist.github.com/zot24/7807652)

...die UK Codes unterscheiden sich auch bei Service: z.B. ist 'UPS Express' in EU 10 und in UK 07 ...laut stackoverflow.

#### Links - Sonstige

UPS Versandkosten checken: [UPS - Calculate Time and Cost](https://wwwapps.ups.com/ctc/)

[UPS Rate Change Information](https://www.ups.com/us/en/shipping/rates-update.page)

[marketplace.magento.com/ups-shipping](https://marketplace.magento.com/ups-shipping.html)

Da sind wohl die besseren Beispielcodes [nerdface]

## Begrifflichkeiten
#### UPS Kit für Entwickler
Folgende APIs werden verwendet:
- Kosten berechnen (Rating.zip)
- Versand (Shipping_Pkg.zip)
- Sendungsverfolgung (Tracking.zip)


**Gemini™** APIs: Optimieren Sie Ihren Luftfrachtversandprozess. Gemini APIs werden nicht verwendet.

## API URLs

#### XML API URLs
[Test - Rating](https://wwwcie.ups.com/ups.app/xml/Rate)

[Production - Rating](https://onlinetools.ups.com/ups.app/xml/Rate)

Folgendes bei google eingeben um alle URLs zu bekommen:
```
# test
site:https://wwwcie.ups.com/ups.app/xml/
# production
site:https://onlinetools.ups.com/ups.app/xml/
```

[...thanks to stackoverflow](https://stackoverflow.com/questions/8894650/ups-api-php-end-point-url)

# stackoverflow
[ups-shipping-api-shipmentconfirmrequest-error](https://stackoverflow.com/questions/20294168/ups-shipping-api-shipmentconfirmrequest-error)
