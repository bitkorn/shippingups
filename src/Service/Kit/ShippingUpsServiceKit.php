<?php

namespace Bitkorn\ShippingUps\Service\Kit;

use Bitkorn\ShippingUps\Service\AbstractShippingUpsService;
use phpDocumentor\Reflection\Types\Mixed_;
use SimpleXMLElement;

class ShippingUpsServiceKit extends AbstractShippingUpsService
{
    /**
     * @var SimpleXMLElement
     */
    protected $shipmentConfirmRequestXML;

    /**
     * @return string
     */
    public function getShipmentConfirmRequestXML(): string
    {
        return $this->shipmentConfirmRequestXML->asXML();
    }

    public function initShipmentConfirmRequestXML(): void
    {
        $this->shipmentConfirmRequestXML = new SimpleXMLElement ("<ShipmentConfirmRequest></ShipmentConfirmRequest>");
        $request = $this->shipmentConfirmRequestXML->addChild('Request');
        $request->addChild('RequestAction', 'ShipConfirm');
        $request->addChild('RequestOption', 'nonvalidate');

        $labelSpecification = $this->shipmentConfirmRequestXML->addChild('LabelSpecification');
        $labelSpecification->addChild('HTTPUserAgent', "");
        $labelPrintMethod = $labelSpecification->addChild('LabelPrintMethod');
        $labelPrintMethod->addChild('Code', 'GIF');
        $labelPrintMethod->addChild('Description', '');
        $labelImageFormat = $labelSpecification->addChild('LabelImageFormat');
        $labelImageFormat->addChild('Code', 'GIF');
        $labelImageFormat->addChild('Description', '');

        $this->endpointUrlAppendage = '/ShipConfirm';
        parent::setShipment($this->shipmentConfirmRequestXML);

        /**
         * @todo ist Prepaid/AccountNumber OK?
         * Prepaid | BillThirdParty | FreightCollect
         * AccountNumber | CreditCard | AlternatePaymentMethod
         */
        $paymentInformation = $this->shipmentXML->addChild('PaymentInformation');
        $prepaid = $paymentInformation->addChild('Prepaid');
        $billShipper = $prepaid->addChild('BillShipper');
        $billShipper->addChild('AccountNumber', $this->upsConfig['shipment']['number']);
    }

    /**
     * @return bool|string
     */
    public function doShipmentConfirmRequest()
    {
        $ch = $this->prepareCurl();
        if (false === $ch) {
            return false;
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->accessRequestXML->asXML() . $this->shipmentConfirmRequestXML->asXML());
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}
