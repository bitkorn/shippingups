<?php

namespace Bitkorn\ShippingUps\Service\Kit;

use Bitkorn\ShippingUps\Service\AbstractUpsService;
use SimpleXMLElement;

/**
 * The Address Validation API validates city, state and ZIP codes in the United States and Puerto Rico.
 *
 * @package Bitkorn\ShippingUps\Service\Kit
 */
class AddressValidationUpsServiceKit extends AbstractUpsService
{

    /**
     * @var SimpleXMLElement
     */
    protected $addressValidationRequestXML;

    public function initAddressValidationRequestXML(): void
    {
        $this->addressValidationRequestXML = new SimpleXMLElement('<AddressValidationRequest></AddressValidationRequest>');
        $request = $this->addressValidationRequestXML->addChild('Request');
        $request->addChild('RequestAction', 'AV');
    }

    public function setAddress(string $city, string $postalCode): void
    {
        $address = $this->addressValidationRequestXML->addChild('Address');
        $address->addChild('City', $city);
        $address->addChild('PostalCode', $postalCode);
    }

    /**
     * @return string accessRequestXML & addressValidationRequestXML
     */
    public function getRequestXml(): string
    {
        return $this->accessRequestXML . $this->addressValidationRequestXML;
    }

}
