# Laminas Module bitkorn/shipping-ups

Implementierung der UPS API zu:
- Adressvalidierung NEIN
  - The Address Validation API validates city, state and ZIP codes in the **United States and Puerto Rico**.
- Kosten berechnen
- Versand
- Sendungsverfolgung

Verwendet werden die XML APIs.

## UPS developer kit - FAQs
Does the Rating API support international rates?
Yes. As long as the shipper number is from the origin country the requests will be validated. 
