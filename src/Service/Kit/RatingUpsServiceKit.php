<?php

namespace Bitkorn\ShippingUps\Service\Kit;

use Bitkorn\ShippingUps\Service\AbstractShippingUpsService;
use SimpleXMLElement;

/**
 * Class RatingUpsService
 * @package Bitkorn\ShippingUps\Service\Kit
 */
class RatingUpsServiceKit extends AbstractShippingUpsService
{

    /**
     * @var SimpleXMLElement
     */
    protected $ratingServiceSelectionRequestXML;

    /**
     * @var
     */
    protected $priceDifferentOriginCountry;

    /**
     * @return string
     */
    public function getRatingServiceSelectionRequestXML(): string
    {
        return $this->ratingServiceSelectionRequestXML->asXML();
    }

    /**
     * @param mixed $priceDifferentOriginCountry
     */
    public function setPriceDifferentOriginCountry($priceDifferentOriginCountry): void
    {
        $this->priceDifferentOriginCountry = $priceDifferentOriginCountry;
    }

    public function initRatingRequestXML(): void
    {
        $this->ratingServiceSelectionRequestXML = new SimpleXMLElement('<RatingServiceSelectionRequest></RatingServiceSelectionRequest>');
        $request = $this->ratingServiceSelectionRequestXML->addChild('Request');
        $request->addChild('RequestAction', 'Rate');
        $request->addChild('RequestOption', 'Rate');

        $this->endpointUrlAppendage = '/Rate';
        parent::setShipment($this->ratingServiceSelectionRequestXML);
    }

    /**
     * @return bool|string
     */
    public function doRatingServiceSelectionRequest()
    {
        $ch = $this->prepareCurl();
        if(false === $ch) {
            return false;
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->accessRequestXML->asXML() . $this->ratingServiceSelectionRequestXML->asXML());
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

}
