[Original Logo](./UPS_logo.svg)

[Original Logo URL](https://www.ups.com/assets/resources/images/UPS_logo.svg)

[wikimedia.org SVG UPS_Logo_Shield_2017](./UPS_Logo_Shield_2017.svg)

[wikimedia.org URL UPS_Logo_Shield_2017](https://upload.wikimedia.org/wikipedia/commons/1/1b/UPS_Logo_Shield_2017.svg)

[wikimedia.org SVG United_Parcel_Service_logo_2014](./United_Parcel_Service_logo_2014.svg)

[wikimedia.org URL United_Parcel_Service_logo_2014](https://upload.wikimedia.org/wikipedia/commons/6/6b/United_Parcel_Service_logo_2014.svg)