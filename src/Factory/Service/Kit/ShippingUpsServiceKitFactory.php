<?php

namespace Bitkorn\ShippingUps\Factory\Service\Kit;

use Bitkorn\ShippingUps\Service\Kit\ShippingUpsServiceKit;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ShippingUpsServiceKitFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ShippingUpsServiceKit();
        $service->setLogger($container->get('logger'));
        $config = $container->get('config');
        $service->setUpsConfig($config['bitkorn_shop_shipping_ups']);
        $service->initShipmentConfirmRequestXML();
        return $service;
    }
}
